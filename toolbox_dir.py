__author__ = 'Loïc POCCARD, Guillaume KERGUERIS'

# imports /
import os
from os import listdir
from shutil import copyfile, copytree, rmtree


class ToolboxDir():
    def __init__(self):
        self.current_dir_path = os.getcwd() # We're picking up where we left off on the hard drive.
        self.result_dir_name = "result"
        self.result_dir_path = os.path.join(self.current_dir_path, self.result_dir_name)
        self.static_dir_name = "static"
        self.static_dir_path = os.path.join(self.current_dir_path, self.static_dir_name)
        self.extract_dir_name = "dir_files"
        self.extract_dir_path = os.path.join(self.current_dir_path, self.extract_dir_name)
        # Reading the file we're trying to retrieve.
        self.list_files_extract_dir = []
        self.list_dirs_extract_dir = []

    def walk_into_extract_files_dirs(self):
        for root, directories, filenames in os.walk(self.extract_dir_path):
            for directory in directories:
                if directory.startswith("."):
                    continue
                else:
                    self.list_dirs_extract_dir.append(os.path.join(root, directory))
            for filename in filenames:
                if filename.startswith(".") or filename.endswith(".pdf") or filename.endswith(".html"):
                    continue
                elif filename.endswith(".md"):
                    self.list_files_extract_dir.append(os.path.join(root, filename))
                else:
                    continue
        print(self.list_dirs_extract_dir)
        print('self.list_files_extracr_dir -> :', self.list_files_extract_dir)


    def basic_result_exec(self):
        """ Basic execution of the setup """
        self.create_res_dir()
        self.copy_static()
        i = 0

        # add commentaire
        #self.current_file = "index.html"
        #b_html = self.create_index_html()
        # Write in index.html
        #with open(os.path.join(self.result_dir_name, 'index.html'), 'w') as f:
            #f.writelines(b_html)

        print(self.list_files_extract_dir)
        for a_file in self.list_files_extract_dir:
            print(a_file)
            self.current_file = a_file
            # add commentaire
            a_html = self.create_full_tab_html()
             # Write in file
            with open(os.path.join(self.result_dir_name, str(i) + 'index.html'), 'w') as f:
                f.writelines(a_html)
            print("mdr")
            i+= 1


    def create_res_dir(self):
        """ Create target directory """
        if(os.path.exists(self.result_dir_path)):
            rmtree(self.result_dir_path)
        try:
            os.mkdir(self.result_dir_name)
            print("Directory " , self.result_dir_name ,  " Created ")
        except FileExistsError:
            print("Directory " , self.result_dir_name ,  " already exists")


    def copy_static(self):
        """ Copy static folder in resulte folder """
        static_path_res = os.path.join(self.result_dir_path, self.static_dir_name)
        copytree(self.static_dir_path, static_path_res)


    def get_current_dir_path(self):
        """ Get current directory """
        return self.current_dir_path

    def changement_path_extract(self, name, path):
        self.extract_dir_name= name
        self.extract_dir_path= path
