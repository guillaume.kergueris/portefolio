__author__ = 'Loïc POCCARD, Guillaume KERGUERIS'

# imports /
import os
import time
import webbrowser
import shutil
import mysql.connector
from tkinter import *
from tkinter import messagebox, filedialog
import subprocess
from toolbox import ToolBox
from toolbox_situations import situation

#########################################################################
#           settings de connection avec notre base de données
#########################################################################
mydb = mysql.connector.connect(host="localhost",
							port="3307",
							user="root",
							password="password",
							database="genurfiles",
							buffered=True,)
class Appli(Tk):
	""" """
	size = 600

	def __init__(self):
		Tk.__init__(self)
		# d'abord on fixe le background afin qu'il soit en arrière plan sur la stack
		self.background_image = PhotoImage(file="giphy.gif")
		#self.background_image.pack(side="LEFT")
		# variable définit sur l'utilisateur à personnaliser son portefolio ou non
		self.personnalisation = False
		# flags des couleurs des différents bouttons afin de déterminer ceux qui sont actif ou non
		self.color_personnalisation = "blue"
		self.color_run = "grey"
		self.color_voir = "grey"

		#variables dossier extraction
		self.extract_dir_name = ""
		self.extract_dir_path = ""

		# dictionnaire avec les informations à rentrer par l'utilisateur afin qu'il personnalise le portefolio
		self.informations = {'nom' : "nom",
						'prenom' : "prenom",
						'hobbies': "hobbies",
						'age' : "age",
						'metier' : "metier"}
		self.storage_key = {} # stocke les objects créés lors de la personnalisation afin de pouvoir intéragir avec
		# import des widgets
		self.login_screen()

	def login_screen(self):
		""" Création du portail pour se connecter"""
		# Création d'un widget Label
		self.Label_titre = Label(self, text = 'Bienvenue dans GenUrFiles', fg = 'black')
		# Positionnement du widget avec la méthode pack()
		self.Label_titre.pack()
		# Création d'un widget Label
		self.Label_demande = Label(self, text = 'Veuillez-vous identifier', fg = 'black')
		# Positionnement du widget avec la méthode pack()
		self.Label_demande.pack()

		#insertion des champs utilisateur
		#username
		username = Entry(self,width=20)
		self.storage_key["username"]= username
		username.pack()
		username.insert(0, "Username")
		password = Entry(self,width=20)

		#password
		self.storage_key["password"]= password
		password.pack()
		password.insert(0, "password")
		self.validate_button = Button(self, text = 'Se connecter', command = self.capture_inputs_login)
		self.validate_button.pack()
		self.inscription_button = Button(self, text = "S'inscrire", command = self.inscription_screen)
		self.inscription_button.pack(side=BOTTOM)

	def inscription_screen(self):
		""" Création de l'écran pour s'inscrire"""
		self.clean_screen()
		# Création d'un widget Label
		self.Label_titre = Label(self, text = "Veuillez remplir le formulaire d'inscription ci-dessous", fg = 'black')
		# Positionnement du widget avec la méthode pack()
		self.Label_titre.pack()

		#insertion des champs utilisateur afin d'inscrire

		#username
		username = Entry(self,width=20)
		self.storage_key["username"]= username
		username.pack()
		username.insert(0, "username")

		#prenom
		prenom = Entry(self,width=20)
		self.storage_key["prenom"]= prenom
		prenom.pack()
		prenom.insert(0, "prenom")

		#nom
		nom = Entry(self,width=20)
		self.storage_key["nom"]= nom
		nom.pack()
		nom.insert(0, "nom")

		#password
		password = Entry(self,width=20)
		self.storage_key["password"]= password
		password.pack()
		password.insert(0, "password")

		#mail
		mail = Entry(self,width=20)
		self.storage_key["mail"]= mail
		mail.pack()
		mail.insert(0, "mail")

		self.validate_button = Button(self, text = 'Valider', command = self.capture_inputs_inscription)
		self.validate_button.pack(side=BOTTOM)

	def situations_screen(self):
		""" Création de l'écran pour gèrer ses situations"""
		self.clean_screen()
		#TODO: insérer un lecteur qui lit les dossiers de situation de l'utilisateur et affiche ses situations en ouvrant les objets
		# stockés

	def create_widgets_menu(self):
		""" ajouts des widgets du menu principal"""

		# Création d'un widget Label
		self.Label1 = Label(self, text = 'Bienvenue dans notre générateur de portefolio', fg = 'black', anchor='nw')
		# Positionnement du widget avec la méthode pack()
		self.Label1.pack(side='left', padx=10, pady=5)

		# Création d'un widget Button (bouton Run)
		self.run_button = Button(self, text = 'Run', command = self.Run, fg=self.color_run)
		self.run_button.pack() # pack gère le positionnement du bouton que l'on vient de créer dans notre affichage

		# Création d'un widget Changer de repertoire d'extraction
		self.dir_choose_button = Button(self, text = 'Selectionner le dossier extraction', command = self.extraction, fg=self.color_personnalisation)
		self.dir_choose_button.pack() # pack gère le positionnement du bouton que l'on vient de créer dans notre affichage

		# Création d'un widget Button (bouton Quitter)
		self.quit_button = Button(self, text="Quitter", command=self.leave)
		self.quit_button.pack(side=BOTTOM, pady=5)

		# button pour personnaliser le portefolio
		self.personal_form = Button(self, text= "Personnaliser son portefolio", command=self.app_form, fg=self.color_personnalisation)
		self.personal_form.pack(side=TOP)

		# button pour creer ses situations
		self.add_situation = Button(self, text="Mes situations", command=self.situations_screen, fg=self.color_personnalisation)
		self.add_situation.pack(side=TOP)


	def Run(self):
		""" Main function that runs all the project """
		if self.personnalisation is False:
			response = messagebox.askokcancel("Portefolio", "Voulez-vous continuez sans avoir personnalisé le portefolio?")
			if response:
				self.color_run = "grey"
				self.color_voir = "blue"
				self.color_personnalisation = "grey"
				objet_toolboxdir = ToolBox()
				self.result_dir_path = objet_toolboxdir.result_dir_path # on ajoute le chemin du dossier de résultat à l'objet
				if self.extract_dir_name != "":
					objet_toolboxdir.changement_path_extract(self.extract_dir_name, self.extract_dir_path)
				objet_toolboxdir.walk_into_extract_files_dirs()
				self.back_menu()
				self.dir_button = Button(self, text="Visionner le résultat dans le dossier", command=self.open_portefolio_dir, fg=self.color_voir)
				self.dir_button.pack()

				# pour ajouter l'ouverture du portefolio
				self.open_navigator = Button(self, text="Voir", command=self.open_portefolio, fg=self.color_voir)
				self.open_navigator.pack()

				# pour ajouter l'enregistrement du portefolio
				self.open_navigator = Button(self, text="Enregistrer dans un dossier personnel", command=self.choose_portefolio_sauvegarde, fg=self.color_voir)
				self.open_navigator.pack()
		if self.personnalisation:
			self.color_run = "grey"
			self.color_voir = "blue"
			objet_toolboxdir = ToolBox()
			self.result_dir_path = objet_toolboxdir.result_dir_path # on ajoute le chemin du dossier de résultat à l'objet
			if self.extract_dir_name != "":
				objet_toolboxdir.changement_path_extract(self.extract_dir_name, self.extract_dir_path)
			objet_toolboxdir.walk_into_extract_files_dirs()
			self.back_menu()
			self.dir_button = Button(self, text="Visionner le résultat dans le dossier", command=self.open_portefolio_dir, fg=self.color_voir)
			self.dir_button.pack()

			# pour ajouter l'ouverture du portefolio
			self.open_navigator = Button(self, text="Voir", command=self.open_portefolio, fg=self.color_voir)
			self.open_navigator.pack()

			# pour ajouter l'enregistrement du portefolio
			self.open_navigator = Button(self, text="Enregistrer dans un dossier personnel", command=self.choose_portefolio_sauvegarde, fg=self.color_voir)
			self.open_navigator.pack()


	def back_menu(self):
		"""Nettoie l'écran actuel et réécrit les widgets du menu par dessus"""
		self.clean_screen()
		self.create_widgets_menu()


	def leave(self):
		"""Quitte le logiciel"""
		reponse = messagebox.askokcancel("leave", "Êtes vous sûr de vouloir quitter?")
		if reponse:
			self.quit()


	def app_form(self):
		"""Formulaire à remplir par l'utilisateur"""
		# on commence par nettoyer l'écran pour replacer des widgets
		self.clean_screen()
		## Création des widgets
		for key in self.informations:
			temp_char = key
			key = Entry(self,width=len(self.informations[temp_char])*3)
			self.storage_key[temp_char]= key
			key.pack(side=TOP)
			key.insert(0, "{0}".format(self.informations[temp_char]))
		self.validate_button = Button(self, text = 'Valider les informations', command = self.capture_inputs_form)
		self.validate_button.pack(side=BOTTOM)


	def capture_inputs_form(self):
		"""Enregistre les informations saisies par l'utilisateur dans le formulaire"""
		print(self.storage_key)
		self.personnalisation = True
		self.color_personnalisation = "grey"
		self.color_run = "blue"
		for key in self.storage_key:
			temp_char = key
			print(self.storage_key[key].get())
			self.informations[temp_char] = self.storage_key[key].get()
		print(self.informations)
		#nettoie le dictionnaire
		self.storage_key.clear()
		self.back_menu()

	def capture_inputs_login(self):
		"""Enregistre les informations saisies par l'utilisateur dans le login verifie en base et affiche le menu si c'est bon"""
		for key in self.storage_key:
			temp_char = key
			if temp_char == "username":
				username = self.storage_key[key].get()
			if temp_char == "password":
				password = self.storage_key[key].get()
		#nettoie le dictionnaire
		print(self.informations)

		# requète de la db
		mycursor = mydb.cursor(dictionary=True)
		# formulation de la requète
		req_connection_client = "SELECT * FROM users where username = '{0}' AND password = '{1}' ".format(username, password)
		# execution de la requète
		mycursor.execute(req_connection_client)
		resultat_connection_client = mycursor.fetchall()
        #print(resultat_connection_client)
		if len(resultat_connection_client) == 0:
			login = False
		else:
			#temp = resultat_connection_client[0]["id"]
			nom = resultat_connection_client[0]["username"]
			login = True
		self.storage_key.clear()
		if login:
			self.back_menu()
		else:
			messagebox.showerror("Erreur", "Connection impossible, identifiants erronés")
			self.clean_screen()
			self.login_screen()

	def capture_inputs_inscription(self):
		"""Enregistre les informations saisies par l'utilisateur dans le fromulaire d'inscription et les enregistre en base"""
		for key in self.storage_key:
			temp_char = key
			if temp_char == "username":
				username = self.storage_key[key].get()
			if temp_char == "password":
				password = self.storage_key[key].get()
			if temp_char == "nom":
				nom = self.storage_key[key].get()
			if temp_char == "prenom":
				prenom = self.storage_key[key].get()
			if temp_char == "mail":
				mail = self.storage_key[key].get()

		mycursor = mydb.cursor()
		req_connection_client = "INSERT INTO `users` (`email`, `username`, `password`, `name`, `first_name`) VALUES('%s', '%s', '%s', '%s', '%s') " % (mail, username, password, nom, prenom)
		mycursor.execute(req_connection_client)
		mydb.commit()
		self.storage_key.clear()#nettoie le dictionnaire
		print(mycursor.rowcount, "record inserted.")
		if mycursor.rowcount is None:
			messagebox.showerror("Erreur", "Erreur lors de l'inscription")
			self.clean_screen()
			self.capture_inputs_inscription()
		else:
			messagebox.showinfo("Inscription réussie", "Veuillez vous connecter")
			self.clean_screen()
			self.login_screen()

	def capture_inputs_situation(self):
		"""TODO: coder creer des objets situations et les sauvegarder"""

	def clean_screen(self):
		""" Nettoie tout l'écran de l'application ( permet de passer à un autre écran) """
		for widgets in self.winfo_children():
			widgets.destroy()


	def open_portefolio(self):
		"""Ouvre le page index du portefolio"""
		if sys.platform.startswith('darwin'):
			url = "file:///" + os.path.join(os.getcwd(), "result", "0index.html")
		else:
			url = os.path.join(os.getcwd(), "result", "0index.html")
		#print(url)
		webbrowser.open(url, new=2)


	def open_portefolio_dir(self):
		""" """
		# faire un chemin personnaliser
		file_to_show = os.path.join(os.getcwd(), "result", "static")
		subprocess.call(["open", "-R", file_to_show])

	def choose_portefolio_sauvegarde(self):
		""" """
		copy_path = filedialog.askdirectory()
		shutil.copytree(self.result_dir_path,os.path.join(copy_path,"result"))

	def extraction(self):
		""""""
		extract_path = filedialog.askdirectory()
		self.extract_dir_name = os.path.split(extract_path)[1]
		self.extract_dir_path = extract_path


if __name__ == "__main__":
	App = Appli()
	App.title("PortfolioGen")
	imgicon = PhotoImage(file=os.path.join(os.getcwd(), "static", "img", "directory.gif"))
	App.call('wm', 'iconphoto', App._w, imgicon)
	App.geometry("{0}x{0}".format(App.size))
	# Lancement du gestionnaire d'événements
	App.mainloop()

