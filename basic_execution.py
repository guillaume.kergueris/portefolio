__author__ = 'Loïc POCCARD, Guillaume KERGUERIS'

# imports /

class BasicExecution(Toolbox):
    def __init__(self):
        pass


    def create_index_html(self): # rename.
            """ Executes all the necessary methods to create an html index """
            top_header_html = self.get_header_top_html()
            bot_header_html = self.get_header_bot_html()
            nav_bar = self.edit_nav_bar_html()
            body_html = self.get_cards_for_index_html()
            footer = self.get_footer_html()
            unindented_html = top_header_html + bot_header_html + nav_bar + body_html + footer
            html = self.indent_html(unindented_html)
            return html


        def create_full_tab_html(self): # raname
            """ Executes all the necessary methods to create an html situation page """
            top_header_html = self.get_header_top_html()
            bot_header_html = self.get_header_bot_html()
            nav_bar = self.edit_nav_bar_html()
            body_html = self.parse_line_to_html()
            end_body_html = self.get_end_body_html()
            footer = self.get_footer_html()
            unindented_html = top_header_html + bot_header_html + nav_bar + body_html + end_body_html + footer
            html = self.indent_html(unindented_html)
            return html