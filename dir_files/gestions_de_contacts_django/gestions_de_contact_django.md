*Situation professionnelle

**Description de la situation :
-Demande de mon n+1

**Description du projet :
-Créer un nouveau contact
-Afficher la liste des contacts
-Rechercher un contact
-Éditer un contact
-Supprimer un contact
-Création d'un annuaire avec Django pour découvrir le framework

**Étapes et déroulement du projet :
-Documentation sur Django
-Suivit d’un cour en ligne sur openclassrooms
-Création du projet Django
-Configuration du serveur et des settings du projet
-Définition des urls pour les différentes vues et de l'application annuaire
-Création de l'application dans le framework Django
-Création de la base de données
-Création de la table contact
-Création des objets contact (nom, prénom, numéro, email)
-Création de contact via ligne de commande python
-Création de la vue view_contact
-Création de la page HTML contact.html
-Création de la vue add_contact
-Création de la page HTML add_contact.html
-Création de la vue delete_contatct

**In Progress :
-Vu pour la recherche de contact
-Vu pour la modification de contact