__author__ = 'Loïc POCCARD, Guillaume KERGUERIS'

# imports /
import time
import datetime
import os
from toolbox_dir import ToolboxDir
import pickle #ouvre nos objets sauvegardés
#import numpy #transforme les données python en images TODO: WTF NUMPY N'EXISTE PAS??

class ToolBox(ToolboxDir):
    def __init__(self):
        self.date = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d-%H:%M:%S')
        ToolboxDir.__init__(self)
        self.current_file = ""
        self.basic_result_exec()


    def create_index_html(self):
        """ Executes all the necessary methods to create an html index """
        top_header_html = self.get_header_top_html()
        bot_header_html = self.get_header_bot_html()
        nav_bar = self.edit_nav_bar_html()
        body_html = self.get_cards_for_index_html()
        footer = self.get_footer_html()
        unindented_html = top_header_html + bot_header_html + nav_bar + body_html + footer
        html = self.indent_html(unindented_html)
        return html


    def create_full_tab_html(self):
        """ Executes all the necessary methods to create an html situation page """
        top_header_html = self.get_header_top_html()
        bot_header_html = self.get_header_bot_html()
        nav_bar = self.edit_nav_bar_html()
        body_html = self.parse_line_to_html()
        end_body_html = self.get_end_body_html()
        footer = self.get_footer_html()
        unindented_html = top_header_html + bot_header_html + nav_bar + body_html + end_body_html + footer
        html = self.indent_html(unindented_html)
        return html


    @staticmethod
    def set_options():
        """ Set user options """
        options_dict = {}
        i = 0
        with open('static/config/config.txt', 'r') as f:
            lines = f.readlines()
            for line in lines:
                try :
                    element = line.split('=')
                    cle = element[0].strip()
                    data = element[1].strip()
                    options_dict[cle]= data
                except:
                    continue
        return options_dict


    @staticmethod
    def get_header_top_html():
        """ Get html header top """
        header_top_html = ['<!-- header top -->\n',
                        '<html lang="fr">\n',
                        '<head>\n',
                        '<meta charset="UTF-8">\n',
                        '<link rel="stylesheet" href="static/css/style.css">\n',
                        '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">\n',
                        '<!-- /.header top -->\n']
        return header_top_html


    def get_header_bot_html(self):
        """ Get html header bottom """
        header_bot_html = []
        option_dict = self.set_options()
        header_bot_html.append('<!-- header bottom -->\n')
        header_bot_html.append('<title>' + option_dict['Title'] + '</title>\n') # ajout du titre du html récupérer avec set_options.
        header_bot_html.append('</head>\n')
        header_bot_html.append('<!-- /.header top -->\n')
        return header_bot_html


    @staticmethod
    def get_nav_bar_html(): # récupère la navbar bootstrap déjà indentée /
        """ Get html navigation bar """
        nav_bar_html = ['<body class="bg">\n',
                    '<!-- navbar -->\n',
                    '<nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar bg-primary color_nav">\n',
                    '<!-- SideNav slide-out button -->\n',
                    '<div class="float-left">\n',
                    '<a href="#" data-activates="slide-out" class="button-collapse"><i class="fas fa-bars"></i></a>\n',
                    '</div>\n',
                    '<!-- Breadcrumb-->\n',
                    '<div class="breadcrumb-dn mr-auto">\n',
                    '<a class="navbar-brand" href="index.html"><img class="logo_portfolio" src="static/img/directory.svg" alt="logo_portfolio"></a>\n',
                    '</div>\n',
                    '<ul class="nav navbar-nav nav-flex-icons ml-auto">\n',
                    '<li class="nav-item">\n',
                    '<a class="nav-link"><i class="fas fa-envelope"></i> <span class="clearfix d-none d-sm-inline-block">Contact</span></a>\n',
                    '</li>\n',
                    '<li class="nav-item">\n',
                    '<a class="nav-link"><i class="far fa-comments"></i> <span class="clearfix d-none d-sm-inline-block">Support</span></a>\n',
                    '</li>\n',
                    '<li class="nav-item">\n',
                    '<a class="nav-link"><i class="fas fa-user"></i> <span class="clearfix d-none d-sm-inline-block">Account</span></a>\n',
                    '</li>\n',
                    '<li class="nav-item dropdown">\n',
                    '<a class="text-light nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true", aria-expanded="false">Dropdown</a>\n',
                    '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">\n',
                    '<a class="dropdown-item" href="#">Action</a>\n',
                    '<a class="dropdown-item" href="#">Another action</a>\n',
                    '<a class="dropdown-item" href="#">Something else here</a>\n',
                    '</div>\n',
                    '</li>\n',
                    '</ul>\n',
                    '</nav>\n',
                    '<!-- /.navbar -->\n']
        return nav_bar_html


    def edit_nav_bar_html(self):
        """ Edit html navigation bar """
        nav_bar_html = self.get_nav_bar_html()
        name_sujet = self.list_files_extract_dir
        i = 0
        while i < len(name_sujet):
            fullhref = self.current_dir_path + "/" + self.extract_dir_name + "/"+ name_sujet[i]
            cutted_name = name_sujet[i]
            i += 1
        return nav_bar_html


    def get_cards_for_index_html(self):
        """ """
        i = 0
        options_dict = self.set_options()
        bg_color = ['primary', 'secondary', 'success', 'danger', 'warning', 'info']
        while i < len(self.list_dirs_extract_dir):
            title_situation = str(os.path.split(self.list_dirs_extract_dir[i])[1])
            title_situation = title_situation.replace('_', ' ')
            description_situation = '' # TODO.
            if i > 5:
                i = 0
            body_html = ['<main role="main">\n',
                            '<div class="container">\n',
                            '<div class="raw">\n',
                            '<div class="offset-lg-2 col-lg-8">\n',
                            '<!-- cards -->\n',
                            '<div class="card-columns">\n']
            body_html.append('<div class="card text-white bg-' + bg_color[i] + ' mb-3" style="max-width_background_image: 18rem;">\n')
            body_html.append('<div class="card-header">' + title_situation + '</div>\n',)
            body_html.append('<div class="card-body">\n')
            body_html.append('<h5 class="card-title">Primary card title</h5>\n')
            body_html.append('<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card\'s content.</p>\n')
            body_html.append('</div>\n')
            body_html.append('</div>\n')
            i += 1
        body_html.append('</div>\n')
        body_html.append('</div>\n')
        body_html.append('</div>\n')
        body_html.append('</div>\n')
        body_html.append('</main>\n')
        body_html.append('<!-- /.cards -->\n')
        body_html.append('</body>\n')

        print('BODY HTML -> :', body_html)
        return body_html


    def parse_line_to_html(self):
        """ Parse options markdown file to html """
        i = 0
        flag_balise_li = False
        end_of_file = False
        body_html = []
        body_html.append('<div class="container" style="overflow:scroll;height:98%;background:rgba(255, 255, 255, 0.486);border-radius: 10px;">\n') # TODO : look this line
        with open(self.current_file, 'r') as f:
            lines = f.readlines()
        tab = [] # on créé un tab qui sera la version strip de lines.
        for x in lines:
            tab.append(x.strip())
        while i < len(tab):
            try:
                after_lines = tab[i + 1]
            except:
                end_of_file = True
            if tab[i][:2] == '**':
                body_html.append('<h2>' + tab[i][2:] + '</h2>\n')
            elif tab[i][:1] == '*':
                body_html.append('<h1>' + tab[i][1:] + '</h1>\n')
            elif tab[i][:1] == '':
                if flag_balise_li == True:
                    body_html.append('</ul>\n')
                    flag_balise_li = False
                body_html.append('</br>\n')
            elif tab[i][:1] == '-':
                if flag_balise_li == False:
                    body_html.append('<ul>\n')
                    body_html.append('<li>' + tab[i][1:] + '</li>\n')
                    flag_balise_li = True
                elif after_lines[:1] != '-' or end_of_file == True:
                    body_html.append('<li>' + tab[i][1:] + '</li>\n')
                    body_html.append('</ul>\n')
                    flag_balise_li = False
                elif flag_balise_li == True:
                    body_html.append('<li>' + tab[i][1:] + '</li>\n')
            else :
                body_html.append('<p>' + tab[i] + '</p>')
            i = i + 1
        body_html.append('<p>' + self.date + '</p>\n')
        return body_html


    @staticmethod
    def get_end_body_html():
        """ Get html end of body """
        end_body_html = ['</div>\n',
                        '</body>\n']
        return end_body_html


    @staticmethod
    def get_footer_html():
        """ Get html footer """
        footer_html = [ '<!-- footer -->\n',
                        '<footer>\n',
                        '<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>\n',
                        '<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>\n',
                        '<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>\n',
                        '<script src="https://cdn.rawgit.com/nnattawat/flip/master/dist/jquery.flip.min.js"></script>\n',
                        '<script type="text/javascript" src="app.js"></script>\n',
                        '<script>\n',
                        '$(".button-collapse").sideNav();\n',
                        '</script>\n',
                        '</footer>\n',
                        '<!-- /.footer -->\n',
                        '</html>']
        return footer_html


    def modify_css(self):
        """ Edit and modify css file """
        i = 0
        options_dict = self.set_options()
        with open('static/css/style.css', 'r+') as f:
            lines = f.readlines()
            lines[5] = 'background-image: url(static/img/' + options_dict['Background image'] + ');'
            lines[13] = 'background-color: ' + options_dict['Background color'] + ';'


    @staticmethod
    def indent_html(html):
        """ Indent html file """
        i = 0
        indentation = 0
        indentation_tab = [0] # tableau des indentations.
        while i < len(html):
            if html[i][:2] != '</' and html[i][-10:] == '</button>\n' or html[i][:4] == '<div' and html[i][-7:] == '</div>\n':
                pass
            elif html[i] == '<li class="nav-item dropdown">\n':
                indentation += 1
            elif html[i][:9] == '</script>' or html[i][:2] == '<!' or html[i][:5] == '</br>' or html[i][:2] == '<a' or html[i][:5] == '<meta' or html[i][:7] == '<script' or html[i][:2] == '<p':
                pass
            elif html[i][:2] == '</':
                indentation_tab[i] = indentation_tab[i] - 1
                indentation -= 1
            elif html[i][:7] == '<title>' and html[i][-8:] == '</title>' or html[i][:3] == '<h' and html[i][:6] != '<head>' or html[i][:2] == '<h' and html[i][:5] != '<html' or html[i][:3] == '<li' and html[i][:21] != '<li class="nav-item">' or html[i][:5] == '<span':
                pass
            elif html[i][:1] == '<':
                indentation += 1
            else:
                print('aled\n')
                print('aled -> :', html[i])
            indentation_tab.append(indentation)
            i += 1
        i = 0
        while i < len(html):
            html[i] = ("\t" * indentation_tab[i]) + html[i]
            i += 1
        return html